<?php
require_once 'Zend/Rest/Controller.php';

/**
 * Controller REST com as características da Akna
 * @author Ezequias Dinella
 *
 */
class Akna_Rest_Controller extends Zend_Rest_Controller
{
    
    // para lidar com o Doctrine
    var $entityManager;
    
    /**
    * (non-PHPdoc)
    * @see Zend_Controller_Action::init()
    */
    public function init()
    {
    
    // autenticação basic
    $authAdapter = new Zend_Auth_Adapter_Http(array(
        'accept_schemes' => 'basic',
    	'realm'          => 'Akna'
        ));
    $basicResolver = new Zend_Auth_Adapter_Http_Resolver_File();
    $basicResolver->setFile('../application/configs/passwdBasic.txt');
    $authAdapter->setBasicResolver($basicResolver);
    
    $authAdapter->setRequest($this->getRequest());
    $authAdapter->setResponse($this->getResponse());
    
    $this->auth = $authAdapter->authenticate();
    
    // define o formato de saída
    $formatoDeSaidaEscolhido = $this->getRequest()->getParam('format');
    $formatoDeSaidaDefault = empty($formatoDeSaidaEscolhido)
        ? 'xml'
        : $formatoDeSaidaEscolhido;
    
    // define as possibilidades de formatos de saída para cada método e escolhe formato definido acima
    $this->_helper->contextSwitch()->addActionContexts(array(
        'index'  => array('xml', 'json'),
        'get'    => array('xml', 'json'),
        'post'   => array('xml', 'json'),
        'put'    => array('xml', 'json'),
        'delete' => array('xml', 'json')
        ))->initContext($formatoDeSaidaDefault);
    
    // doctrine
    $registry = Zend_Registry::getInstance();
    $this->entityManager = $registry->entityManager;
    }
    
    /**
     * (non-PHPdoc)
     * @see Zend_Rest_Controller::indexAction()
     */
    public function indexAction()
    {
        $this->view->code = 501;
        $this->view->message = 'Method not implemented';
        $this->getResponse()->setHttpResponseCode($this->view->code);
        $this->_helper->viewRenderer('status', null, true);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Rest_Controller::getAction()
     */
    public function getAction()
    {
        $this->view->code = 501;
        $this->view->message = 'Method not implemented';
        $this->getResponse()->setHttpResponseCode($this->view->code);
        $this->_helper->viewRenderer('status', null, true);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Rest_Controller::postAction()
     */
    public function postAction()
    {
        $this->view->code = 501;
        $this->view->message = 'Method not implemented';
        $this->getResponse()->setHttpResponseCode($this->view->code);
        $this->_helper->viewRenderer('status', null, true);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Rest_Controller::putAction()
     */
    public function putAction()
    {
        $this->view->code = 501;
        $this->view->message = 'Method not implemented';
        $this->getResponse()->setHttpResponseCode($this->view->code);
        $this->_helper->viewRenderer('status', null, true);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Rest_Controller::deleteAction()
     */
    public function deleteAction()
    {
        $this->view->code = 501;
        $this->view->message = 'Method not implemented';
        $this->getResponse()->setHttpResponseCode($this->view->code);
        $this->_helper->viewRenderer('status', null, true);
    }

}